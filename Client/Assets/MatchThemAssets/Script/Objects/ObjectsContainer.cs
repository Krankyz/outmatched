﻿using System.Threading.Tasks;
using UnityEngine;

public class ObjectsContatiner<T>
{
    private T[,] objectsContainer;

    public int maxItems { get; set; }

    public void Init(int sizeX, int sizeY)
    {
        //Init and create the array
        objectsContainer = new T[sizeX, sizeY];

        //save the max size
        maxItems = sizeX * sizeY;
    }

    public void AddToContainer(T item, int posX, int posY)
    {
        //Adds object to the container
        objectsContainer[posX, posY] = item;
    }

    Task<T[,]> ContainerASYNCAdd(T item, int posX, int posY)
    {
        //Update the container
        AddToContainer(item, posX, posY);
        return Task.FromResult(objectsContainer);
    }

    public async void AddToContainerASYNC(T item, int posX, int posY)
    {
        //Asynchronous container add begin
        await ContainerASYNCAdd(item, posX, posY);
    }

    public T GetFromContainer(int posX, int posY)
    {
        T item = objectsContainer[posX, posY];
        return item;
    }

    public int GetCount()
    {
        return objectsContainer.Length;
    }

    public int GetContainerUpperBound(int dimension)
    {
        return objectsContainer.GetUpperBound(dimension);
    }
}