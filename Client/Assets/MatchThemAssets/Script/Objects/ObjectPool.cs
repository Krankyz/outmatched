﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<K,T>
{
    private ConcurrentDictionary<K, T> objectsPool = new ConcurrentDictionary<K,T>();
    private int counter = 0;

    public int maxItems { get; set; }

    public void AddToPool(K index, T item)
    {
        if(objectsPool.Count < maxItems)
        {
            if(objectsPool.TryAdd(index, item))
            {
                counter++;
            }
        }
        else
        {
            Debug.LogWarning("[ObjectPool] Reached maximum capacity for object pool");
        }
    }

    public T GetFromPool(K index)
    {
        T item;
        if (objectsPool.TryGetValue(index, out item))
        {
            return item;
        }
        return item;
    }

    //Removes cell object from the pool and returns the cell info
    public T RemoveFromPool(K index)
    {
        T item;
        if (objectsPool.TryRemove(index, out item))
        {
            counter--;
            return item;
        }
        return item;
    }

    //Removes cell object from the pool but discards the returned object
    public bool RemoveFromPoolNoReturn(K index)
    {
        if(index == null)
        {
            return false;
        }

        if (objectsPool.TryRemove(index, out _))
        {
            counter--;
            return true;
        }

        return false;
    }

    public bool UpdatePool(K index, T newValue, T OldValue)
    {
        return objectsPool.TryUpdate(index, newValue, OldValue);
    }

    public int GetCount()
    {
      return objectsPool.Count;
    }

    public KeyValuePair<K,T>[] GetArray()
    {
        var array = objectsPool.ToArray();
        return array;
    }

}
