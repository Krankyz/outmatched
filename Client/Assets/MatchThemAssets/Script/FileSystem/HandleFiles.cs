﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Threading.Tasks;

public class HandleFiles : MonoBehaviour
{
    public static async Task<string> ReadAllLinesAsync(string path)
    {
        string resuslt = "";
        if (File.Exists(path))
        {
            StreamReader reader = File.OpenText(path);
            if (reader != null)
            {
                Debug.Log("Reading text file at: " + " [ " + path + " ] ");
                resuslt = await reader.ReadToEndAsync();
                reader.Close();
            }
        }

        if (resuslt == "")
        {
            Debug.Log("Text File at: " + " [ " + path + " ] is empty");
        }

        return resuslt;
    }

    public static async void WriteAllLinesAsync(string path, string[] message)
    {
        //Open the test file for editing
        StreamWriter writer = File.CreateText(path);

        if (writer != null)
        {
            Debug.Log("ASYNC Writing to file at: " + " [ " + path + " ] begins");

            for (int i = 0; i < message.Length; i++)
            {
                await writer.WriteLineAsync(message[i]);
                Debug.Log("Message: " + " [ " + message[i] + " ] has been written to file at " + " [ " + path + " ]");
            }

            Debug.Log("ASYNC Writing to file at: " + " [ " + path + " ] Completed");
            writer.Close();

#if UNITY_EDITOR
            //Re-import the file to update the reference in the editor
            AssetDatabase.ImportAsset(path);
            Resources.LoadAsync<TextAsset>(path);
#endif    
        }
    }

    public static bool CreateFile(string path)
    {
        if (!File.Exists(path))
        {
            Debug.Log("Creating file at: " + " [ " + path + " ] ");
            StreamWriter writer = File.CreateText(path);
            if (writer != null)
            {
                writer.Close();
                return true;
            }
            return false;
        }
        else
        {
            Debug.Log("File at: " + " [ " + path + " ] already exists");
            return true;
        }
    }
}
