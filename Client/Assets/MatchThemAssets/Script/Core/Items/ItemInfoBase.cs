﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInfoBase : MonoBehaviour
{
    //Name of the item
    public string itemName;

    //Max value this item can reach
    public int currentChargeValue;

    //Max value this item can reach
    public int maxChargeValue;

    //The name of the icon for this item
    public string iconName;

    //Miscellaneous modifier for the item
    public int miscModifier;

    //The origin this item is related to
    public ETokenOrigins itemOrigin;
}
