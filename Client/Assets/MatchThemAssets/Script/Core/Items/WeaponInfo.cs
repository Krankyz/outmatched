﻿

public class WeaponInfo : ItemInfoBase
{
    //Base damage of the weapon
    public int baseDamage;

    //Constructors
    public WeaponInfo() { }

    public WeaponInfo(string _itemName, int _currentChargeValue, int _maxChargeValue, string _iconName, int _miscModifier, ETokenOrigins _itemOrigin, int _baseDamage)
    {
        itemName = _itemName;
        currentChargeValue = _currentChargeValue;
        maxChargeValue = _maxChargeValue;
        iconName = _iconName;
        miscModifier = _miscModifier;
        itemOrigin = _itemOrigin;
        baseDamage = _baseDamage;
    }
}
