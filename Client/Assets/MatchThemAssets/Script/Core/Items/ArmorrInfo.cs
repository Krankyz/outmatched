﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorrInfo : ItemInfoBase
{
    //The base health amount of the armor
    public int baseHealthIncrease;

    //The base health amount duration of the armor
    public int baseHealthDuration;

    //The max health amount of the armor
    public int maxHealthIncrease;

    //The max health amount duration of the armor
    public int maxHealthDuration;

    //Constructors
    public ArmorrInfo() { }

    public ArmorrInfo(string _itemName, int _currentChargeValue, int _maxChargeValue, string _iconName, int _miscModifier, ETokenOrigins _itemOrigin, int _baseHealthIncrease, int _baseHealthDuration, int _maxHealthIncrease, int _maxHealthDuration)
    {
        itemName = _itemName;
        currentChargeValue = _currentChargeValue;
        maxChargeValue = _maxChargeValue;
        iconName = _iconName;
        miscModifier = _miscModifier;
        itemOrigin = _itemOrigin;
        baseHealthIncrease = _baseHealthIncrease;
        baseHealthDuration = _baseHealthDuration;
        maxHealthIncrease = _maxHealthIncrease;
        maxHealthDuration = _maxHealthDuration;
    }
}
