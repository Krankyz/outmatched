﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldInfo : ItemInfoBase
{
    //The base barrier value of the shield
    public int baseBarrierValue;

    //The base barrier duration of the shield
    public int baseBarrierDuration;

    //Constructors
    public ShieldInfo() { } 

    public ShieldInfo(string _itemName, int _currentChargeValue, int _maxChargeValue, string _iconName, int _miscModifier, ETokenOrigins _itemOrigin, int _baseBarrierValue, int _baseBarrierDuration)
    {
        itemName = _itemName;
        currentChargeValue = _currentChargeValue;
        maxChargeValue = _maxChargeValue;
        iconName = _iconName;
        miscModifier = _miscModifier;
        itemOrigin = _itemOrigin;
        baseBarrierValue = _baseBarrierValue;
        baseBarrierDuration = _baseBarrierDuration;
    }
}
