﻿
//Token Enums

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum ETokenOrigins
{
    Origin_None,
    Origin_Fierce,
    Origin_Bright,
    Origin_Savage,
    Origin_Mighty,
    Origin_Lethal,
}

[Serializable]
public enum ETokenColour
{
    Colour_None,
    Colour_Red,
    Colour_Yellow,
    Colour_Green,
    Colour_Blue,
    Colour_Black,
}

[Serializable]
public enum ETokenType
{
    Type_None,
    Type_Orb,
    Type_Crown,
    Type_Dragon,
}

//Equipment Enums
[Serializable]
public enum EItemType
{
    Type_None,
    Type_Weapon,
    Type_Armour,
    Type_Shield,
}

[Serializable]
public enum EItemRarity
{
    IRarity_None,
    IRarity_Common,
    IRarity_Rare,
    IRarity_Epic,
    IRarity_Legendary,
    IRarity_Mythic,
}

[Serializable]
public struct Tokens_Info
{
    //X position of the token on the game board
    public int posX;

    //Y position of the token on the game board
    public int posY;

    //GameObject for the pieces
    public GameObject tokenGameObject;

    //Set if this piece is empty or not
    public bool bIsAnEmptyPiece;

    //Origin of the token
    public ETokenOrigins tokenOrigin;

    //Colour of the token
    public ETokenColour tokenColour;

    //The token type
    public ETokenType tokenType;

    //A vector that stores the strength of this token in reference to tis origin 
    public List<ETokenOrigins> TokenOriginStrengths;

    //Constructor
    public Tokens_Info(int _x, int _y, GameObject _gameObject, bool _bIsAnEmptyPiece, ETokenOrigins _tokenOrigin, ETokenColour _tokenColour, ETokenType _tokenType, List<ETokenOrigins> _TokenOriginStrengths)
    {
        posX = _x;
        posY = _y;
        tokenGameObject = _gameObject;
        bIsAnEmptyPiece = _bIsAnEmptyPiece;
        tokenOrigin = _tokenOrigin;
        tokenColour = _tokenColour;
        tokenType = _tokenType;
        TokenOriginStrengths = _TokenOriginStrengths;

    }

    public void Init(int _x, int _y, GameObject _gameObject, bool _bIsAnEmptyPiece, ETokenOrigins _tokenOrigin, ETokenColour _tokenColour, ETokenType _tokenType, List<ETokenOrigins> _TokenOriginStrengths)
    {
        posX = _x;
        posY = _y;
        tokenGameObject = _gameObject;
        bIsAnEmptyPiece = _bIsAnEmptyPiece;
        tokenOrigin = _tokenOrigin;
        tokenColour = _tokenColour;
        tokenType = _tokenType;
        TokenOriginStrengths = _TokenOriginStrengths;

    }

    public void Init(int _x, int _y, GameObject _gameObject, bool _bIsAnEmptyPiece, ETokenOrigins _tokenOrigin, ETokenColour _tokenColour, ETokenType _tokenType)
    {
        posX = _x;
        posY = _y;
        tokenGameObject = _gameObject;
        bIsAnEmptyPiece = _bIsAnEmptyPiece;
        tokenOrigin = _tokenOrigin;
        tokenColour = _tokenColour;
        tokenType = _tokenType;
    }

    public void Init(int _x, int _y, GameObject _gameObject, bool _bIsAnEmptyPiece)
    {
        posX = _x;
        posY = _y;
        tokenGameObject = _gameObject;
        bIsAnEmptyPiece = _bIsAnEmptyPiece;
    }
}