﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GearList : MonoBehaviour
{

    bool panelIsActive = false;
    public GameObject mainPanel;
    public RectTransform parentRectTransform;


    public void ControlPanel(int index)
    {
        if (mainPanel == null)
        {
            Debug.LogError("Panel game object is NULL. Please check that the panel object is set");
            return;
        }

        //First update the panel state
        if (mainPanel.activeSelf)
        {
            panelIsActive = true;
        }
        else
        {
            panelIsActive = false;
        }

        //Check if the panel is active or not
        if (panelIsActive)
        {
            //Hide panel if already active
            mainPanel.SetActive(false);
        }
        else
        {
            //set the item list panel to active
            mainPanel.SetActive(true);

            ////Show the items
            //LoadAndShowItems(index);
        }
           
    }

    //private void LoadAndShowItems(int index)
    //{
    //    //Create new list to store active items
    //    CurrentActiveItems = new List<ItemInfo>();

    //    //get the item info for the players
    //    List<ItemInfo> AllGearInfo = GameInstance.GetPlayerItemsInfo();

    //    //Check if the data is empty
    //    if (AllGearInfo.Count <= 0)
    //        throw new UnityException("The list for head hear pieces is empty");

    //    //Get the piece type the player has clicked so we know what sort of items to display
    //    GameInstance.GearPieceType _gearPieceType = (GameInstance.GearPieceType)index;

    //    //Counter to keep track of when 3 items have been displayed in a row
    //    int counter = 1;

    //    //Stores the anchor positions of the previous item
    //    Vector2 _oldMax = Vector2.zero;
    //    Vector2 _oldMin = Vector2.zero;

    //    //Stores the anchor position of every first item in a row
    //    Vector2 _oldMinForY = Vector2.zero;

    //    for (int i = 0; i < AllGearInfo.Count; i++)
    //    {
    //        if (AllGearInfo[i].GearType == _gearPieceType)
    //        {
    //            //We create the objects to display
    //            ItemInfo _icon = Instantiate(IconPrefab, parentRectTransform);

    //            _icon.ItemName = AllGearInfo[i].ItemName;
    //            _icon.GearType = AllGearInfo[i].GearType;
    //            _icon.IconName = AllGearInfo[i].IconName;

    //            string _IconPath = Path.Combine(Application.streamingAssetsPath, "Icons");
    //            _IconPath += ".png";
    //            ;
    //            Texture2D tex = new Texture2D(2, 2);
    //            byte[] _fileData = File.ReadAllBytes(_IconPath);
    //            if (tex.LoadImage(_fileData) && _icon.IconIamge != null)
    //            {
    //                _icon.IconIamge.sprite = Sprite.Create(tex, _icon.ParentRectTransform.rect, new Vector2(0.5f, 0.5f));
    //            }
    //            else
    //            {
    //                Debug.LogError("Texture has failed to load or no image is found");
    //            }

    //            _icon.HPModifier = AllGearInfo[i].HPModifier;
    //            _icon.Damage = AllGearInfo[i].Damage;
    //            _icon.DamageBonus = AllGearInfo[i].DamageBonus;

    //            //reset old max and min when moving to next line
    //            if (counter == 4)
    //            {
    //                _oldMax = Vector2.zero;
    //                _oldMin = Vector2.zero;
    //            }

    //            //Update X position
    //            if (_icon != null && _oldMax != Vector2.zero)
    //            {
    //                _icon.ParentRectTransform.anchorMin = new Vector2(_oldMax.x + 0.09f, _oldMin.y);
    //                _icon.ParentRectTransform.anchorMax = new Vector2(_icon.ParentRectTransform.anchorMin.x + 0.23f, _oldMax.y);
    //            }

    //            //Update y position when moving to next line
    //            if (counter == 4)
    //            {
    //                if (_icon != null && _oldMinForY != Vector2.zero)
    //                {
    //                    //for Y
    //                    _icon.ParentRectTransform.anchorMax = new Vector2(_icon.ParentRectTransform.anchorMax.x, _oldMinForY.y - 0.05f);
    //                    _icon.ParentRectTransform.anchorMin = new Vector2(_icon.ParentRectTransform.anchorMin.x, _icon.ParentRectTransform.anchorMax.y - 0.1f);
    //                }
    //                counter = 1;
    //            }

    //            _oldMax = _icon.ParentRectTransform.anchorMax;
    //            _oldMin = _icon.ParentRectTransform.anchorMin;

    //            if (counter == 1)
    //            {
    //                _oldMinForY = _icon.ParentRectTransform.anchorMin;
    //            }

    //            counter++;

    //            //Update the current active items
    //            CurrentActiveItems.Add(_icon);
    //        }
    //    }
    //}

    //private void UnloadAndHideItems()
    //{
    //    if(CurrentActiveItems.Count > 1)
    //    {
    //        for(int i=0; i<CurrentActiveItems.Count; i++)
    //        { 
    //            if(CurrentActiveItems[i].IconIamge.sprite.texture != null)
    //            {
    //                Destroy(CurrentActiveItems[i].IconIamge.sprite.texture);
    //            }
    //        }
    //    }
    //}
}
