﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Holoville.HOTween;

public class GameMode : MonoBehaviour
{
    public enum PieceDirection
    {
        NONE,
        STATIONARY,
        UP,
        DOWN,
        LEFT,
        RIGHT,
    }

    [Serializable]
    public struct SwipeMovementInfo
    {
        //GameObject for the pieces
        public PieceDirection Direction;

        //First Piece position on the grid
        public Vector2 FirstPiecePos;

        public SwipeMovementInfo(PieceDirection theDirection, Vector2 theFirstPiecePos)
        {
            Direction = theDirection;
            FirstPiecePos = theFirstPiecePos;
        }
    }

    Vector2 firstPiecePressed;
    Vector2 SecondPiecePressed;

    //After destroying object they are replaced with this one so we will replace them after with new ones
    public TokensInfo emptyGridPiece;

    //The first object selected
    private GameObject firstObject;

    //The second object selected
    private GameObject secondObject;

    //The object we want to use in the effect of shining stars 
    public GameObject ShiningStarsparticleEffect;

    //The gameobject of the effect when the objects are matching
    public GameObject pieceMatchparticleEffect;

    //the sound effect when matched tiles are found
    public AudioClip pieceMatchSound;

    //The grid number of cell horizontally
    public int gridWidth;

    //The grid number of cell vertically
    public int gridHeight;

    //Indicate if we can switch diagonally
    public bool canSwapDiagonally = false;

    //The amount to add when a match is found
    public int scoreIncrement;

    //The actual total score
    private int scoreTotal = 0;

    //The list of tiles we cant to see in the game you can replace them in unity's inspector and choose all what you want
    public TokensInfo[] listOfAvailablePieces;

    //The list of effect objects we want to use in the effect of shining stars 
    private List<GameObject> particleEffectsList = new List<GameObject>();

    //Create the object container
    private ObjectsContatiner<TokensInfo> ObjectsContainer;

    private bool BoardHasBeenLoaded = false;

    // Start is called before the first frame update
    void Start()
    {
        //Create the object pool
        ObjectsContainer = new ObjectsContatiner<TokensInfo>();

        //Init the container
        ObjectsContainer.Init(gridWidth, gridHeight);

        //Create the game pieces asynchronously
        InitGamePiecesAsync();

        //Begin showing the effects
        StartCoroutine(BeginShapeEffect(3f));
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
#else
        if (Input.touchCount > 0)
        {
#endif
        }

        if (BoardHasBeenLoaded)
        {
            CalcualteAndSwap();
            CalcualteAndFindMatch();
        }
    }

    //Async caller function
    async void InitGamePiecesAsync()
    {
        //Get the pieces array ASYNC
        await InitGamePieces();
    }

    //Async task to populate the game pieces
    Task<ObjectsContatiner<TokensInfo>> InitGamePieces()
    {
        int posx = -7;
        int posy = 1;

        //Creating the gems from the list of gems passed in parameter
        for (int i = 0; i <= gridWidth - 1; i++)
        {
            //posx += 1;
            //if (posy > gridHeight - 1)
            //{
            //    posy = 1;
            //}

            for (int j = 0; j <= gridHeight - 1; j++)
            {
                posy += 1;
                //Get a random piece from the list of prefabs
                TokensInfo _randomPiece = listOfAvailablePieces[UnityEngine.Random.Range(0, listOfAvailablePieces.Length)];
                
                //Reinstated a gameobject using that random piece
                GameObject _aPieceGameObject = Instantiate(_randomPiece.gameObject, new Vector3(i, j, 0), transform.rotation);

                if (_aPieceGameObject != null)
                {
                    //Update the main info
                    _randomPiece._tokenInfo.Init(i,j, _aPieceGameObject, false);

                    //Add to the object pool
                    if (ObjectsContainer != null)
                    {
                        ObjectsContainer.AddToContainer(_randomPiece, _randomPiece._tokenInfo.posX, _randomPiece._tokenInfo.posY);
                    }

                    Debug.LogWarning("Object name is: " + _randomPiece._tokenInfo.tokenGameObject.name);
                }
                else
                {
                    Debug.LogError("Failed to instantiate game object");
                }
            }
        }

        BoardHasBeenLoaded = true;
        return Task.FromResult(ObjectsContainer);
    }

    //Instantiate the star objects
    IEnumerator BeginShapeEffect(float delay)
    {
        yield return new WaitForSeconds(delay);
        ShapeEffectAsync();

        //Restart the Coroutine
        StartCoroutine(BeginShapeEffect(3f));
    }

    //Async caller function for swap effect
    async void ShapeEffectAsync()
    {
        //Get Update the pieces Async
        await ShapeEffect();
    }

    //Function that performs the shape effect on the board every 3 seconds asynchronously
    Task<List<GameObject>> ShapeEffect()
    {
        if (particleEffectsList.Count > 0)
        {
            foreach (var row in particleEffectsList)
            {
                Destroy(row);
            }

            //Clear the array
            particleEffectsList.Clear();
        }

        for (int i = 0; i <= 2; i++)
        {
            if (ObjectsContainer != null)
            {
                if (ShiningStarsparticleEffect != null && ObjectsContainer.GetCount() > 0)
                {

                    //set the position and z rotation values
                    float posX = UnityEngine.Random.Range(0, ObjectsContainer.GetContainerUpperBound(0) + 1);
                    float posY = UnityEngine.Random.Range(0, ObjectsContainer.GetContainerUpperBound(1) + 1);
                    float rotZ = UnityEngine.Random.Range(0, 1000f);

                    //Instantiate the effect objects
                    GameObject effectrObject = Instantiate(ShiningStarsparticleEffect, new Vector3(posX, posY, -1), new Quaternion(0, 0, rotZ, 100));

                    //Add to the array
                    if (effectrObject != null)
                    {
                        particleEffectsList.Add(effectrObject);
                    }
                }
            }
        }

        return Task.FromResult(particleEffectsList);
    }

    // Swap Two pieces, it swaps the position of two pieces in the grid array
    void SwapPieces(GameObject firstPiece, GameObject secondPiece, ref ObjectsContatiner<TokensInfo> gameObjectContainer)
    {
        //Make sure the game objects are not null
        if(firstPiece == null || secondPiece == null)
        {
            Debug.LogError("A piece is null. it shouldn't be here");
            return;
        }

        //Get the piece info from the current cell
        TokensInfo firstokenInfo = gameObjectContainer.GetFromContainer((int)firstPiece.transform.position.x, (int)firstPiece.transform.position.y);
        TokensInfo secondtokenInfo = gameObjectContainer.GetFromContainer((int)secondPiece.transform.position.x, (int)secondPiece.transform.position.y);

        //If they are both the same we can proceed to swap
        if (firstokenInfo.gameObject != null && secondtokenInfo.gameObject != null)
        {
            Debug.Log("Swapping objects: FirstObj: " + firstokenInfo._tokenInfo.tokenGameObject.name + " SecondObj: " + secondtokenInfo._tokenInfo.tokenGameObject.name);

            //Add to the pool ASYNC
            gameObjectContainer.AddToContainerASYNC(secondtokenInfo, (int)firstPiece.transform.position.x, (int)firstPiece.transform.position.y);

            //Add to the pool
            gameObjectContainer.AddToContainerASYNC(firstokenInfo, (int)secondPiece.transform.position.x, (int)secondPiece.transform.position.y);

            //play the swap animation
            SwapPieceAnimation(firstPiece.transform, secondPiece.transform);

        }
    }

    // Swap Motion Animation, to animate the switching arrays
    void SwapPieceAnimation(Transform FirstPiece, Transform SecondPiece)
    {
        //Get the local position of the pieces that are getting swapped
        Vector3 firstPiecePos = FirstPiece.localPosition;
        Vector3 secondPiecePos = SecondPiece.localPosition;

        //Animation for second piece
        TweenParms parms = new TweenParms().Prop("localPosition", secondPiecePos).Ease(EaseType.EaseOutQuart);
        HOTween.To(FirstPiece, 0.25f, parms).WaitForCompletion();

        //Animation for first piece
        parms = new TweenParms().Prop("localPosition", firstPiecePos).Ease(EaseType.EaseOutQuart);
        HOTween.To(SecondPiece, 0.25f, parms).WaitForCompletion();
    }

    //Function to calculate if there has been a move
    void CalcualteAndSwap()
    {
        //Boolean to control if there's a swap allowed or not
        bool shouldPerformSwap = false;

        //Get the player's swipe direction
        SwipeMovementInfo matchDirection = CheckBoardStatus();

        if (matchDirection.Direction != PieceDirection.NONE)
        {
            if (HOTween.GetTweenInfos() == null)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(matchDirection.FirstPiecePos), Vector2.zero);
                if (hit.transform != null)
                {
                    //To know if the user already selected a tile or not yet
                    if (firstObject == null)
                    {
                        //set the first game object
                        firstObject = hit.transform.gameObject;

                        Debug.Log("First Object is: " + firstObject.name);

                        //Get the direction to determine the second object
                        if (matchDirection.Direction != PieceDirection.STATIONARY)
                        {
                            Vector3 hit2Position = hit.transform.position;
                            switch (matchDirection.Direction)
                            {
                                case PieceDirection.UP:
                                    hit2Position.y++; break;
                                case PieceDirection.DOWN:
                                    hit2Position.y--; break;
                                case PieceDirection.LEFT:
                                    hit2Position.x--; break;
                                case PieceDirection.RIGHT:
                                    hit2Position.x++; break;
                            }

                            //Get second game object
                            RaycastHit2D hit2 = Physics2D.Raycast(hit2Position, Vector2.zero);

                            if (hit2.transform != null)
                            {
                                //save the second object and update should perform swap
                                secondObject = hit2.transform.gameObject;

                                Debug.Log("Second Object is: " + secondObject.name);
                                shouldPerformSwap = true;
                            }
                        }
                    }
                    else
                    {
                        //check if the second object hasn't been updated
                        secondObject = hit.transform.gameObject;
                        shouldPerformSwap = true;
                    }

                    //If two pieces have been selected by the player. Then we proceed and show the animation as well as continue the move
                    if (shouldPerformSwap)
                    {
                        //Getting the distance between the 2 tiles
                        var distance = firstObject.transform.position - secondObject.transform.position;

                        //Testing if the 2 tiles are next to each others otherwise we will not swap them 
                        if (Mathf.Abs(distance.x) <= 1 && Mathf.Abs(distance.y) <= 1)
                        {
                            //If we don't want the player to swap diagonally
                            if (!canSwapDiagonally)
                            {
                                if (distance.x != 0 && distance.y != 0)
                                {
                                    //Remove indicator and reset the first and second selected pieces game object
                                    firstObject = null;
                                    secondObject = null;
                                    return;
                                }
                            }

                            //swap the pieces and animate the transition
                            SwapPieces(firstObject, secondObject, ref ObjectsContainer);
                        }
                        else
                        {
                            //Reset the first and second selected pieces game object
                            firstObject = null;
                            secondObject = null;
                        }
                    }
                }
            }
        }
    }

    void CalcualteAndFindMatch()
    {
        //We find a match when there isn't an animation playing
        if (HOTween.GetTweenInfos() == null)
        {
            //See if there is a match
            var piecesMatched = FindAMatch(ref ObjectsContainer);

            //If greater than 0, there is a match
            if (piecesMatched.Count > 0)
            {
                //Update the scores
                scoreTotal += piecesMatched.Count * scoreIncrement;

                foreach (TokensInfo matchedGameObjects in piecesMatched)
                {
                    if (pieceMatchSound != null)
                    {
                        //Playing the matching sound
                        GetComponent<AudioSource>().PlayOneShot(pieceMatchSound);
                    }

                    if (pieceMatchparticleEffect != null)
                    {
                        //Create a temp particle effect game object for the pieces
                        var tempParticleGameobject = Instantiate(pieceMatchparticleEffect, new Vector3(matchedGameObjects._tokenInfo.tokenGameObject.transform.position.x, matchedGameObjects._tokenInfo.tokenGameObject.transform.position.y, -2), transform.rotation);

                        //Destroy the temp particle effect after 1 second
                        Destroy(tempParticleGameobject, 1f);

                        //Instantiate an empty piece game object
                        GameObject _emptyPieceObject = Instantiate(emptyGridPiece.gameObject, new Vector3(matchedGameObjects._tokenInfo.tokenGameObject.transform.position.x, matchedGameObjects._tokenInfo.tokenGameObject.transform.position.y, -1), transform.rotation);

                        //Add to the object pool
                        if (ObjectsContainer != null)
                        {
                            //Update the main info
                            emptyGridPiece._tokenInfo.Init((int)matchedGameObjects._tokenInfo.tokenGameObject.transform.position.x, (int)matchedGameObjects._tokenInfo.tokenGameObject.transform.position.y, _emptyPieceObject, true);

                            //Add to the container
                            ObjectsContainer.AddToContainerASYNC(emptyGridPiece, emptyGridPiece._tokenInfo.posX, emptyGridPiece._tokenInfo.posY);

                            Debug.Log("Empty piece replacement");
                        }

                        //Performs clean up
                        Destroy(matchedGameObjects._tokenInfo.tokenGameObject, 0.1f);
                    }
                }

                //Update the and them to null since they have been swapped and marked for deletion
                firstObject = null;
                secondObject = null;

                //Update game board tiles and update and delete pieces that have been marked for deletion
                UpdateEmptySpaces(ref ObjectsContainer);
            }
            else
            {
                Debug.Log("Piece count is less that 0");

                //This means no matches were found
                if (firstObject != null && secondObject != null)
                {
                    //swap the pieces and animate the transition
                    SwapPieces(firstObject, secondObject, ref ObjectsContainer);

                    //Update the and them to null since they have been swapped to original position
                    firstObject = null;
                    secondObject = null;
                }
            }

            //Clear the list when we are done processing the info from it
            piecesMatched.Clear();
        }

        //Update the text with the scores
        GetComponent<TextMesh>().text = scoreTotal.ToString();
    }

    //Checks the board status to see if player performed any swaps
    SwipeMovementInfo CheckBoardStatus()
    {
        //Update first piece position
        if (Input.GetMouseButtonDown(0))
        {
            //save and update the position selected
            firstPiecePressed = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }

        //Can only update the second piece if mouse went up
        if (Input.GetMouseButtonUp(0))
        {
            //save and update the position selected
            SecondPiecePressed = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //Create vector from the two points
            Vector2 currentSwipe = new Vector2(SecondPiecePressed.x - firstPiecePressed.x, SecondPiecePressed.y - firstPiecePressed.y);

            //Normalize the 2d vector
            currentSwipe.Normalize();

            //get the direction of the swipe
            var swipeDirection = MovePiece(currentSwipe);

            //Populate  the  struct
            SwipeMovementInfo MoveInfo = new SwipeMovementInfo(swipeDirection, firstPiecePressed);

            return MoveInfo;
        }

        Debug.Log("Returned an empty move");
        return new SwipeMovementInfo(PieceDirection.NONE, Vector2.zero);
    }

    PieceDirection MovePiece(Vector2 swipe)
    {
        //swipe upwards
        if (swipe.y > 0 && swipe.x > -0.5f && swipe.x < 0.5f)
        {
            Debug.Log("up swipe");  
            return PieceDirection.UP;
        }
        //swipe down
        if (swipe.y < 0 && swipe.x > -0.5f && swipe.x < 0.5f)
        {
            Debug.Log("down swipe");
            return PieceDirection.DOWN;
        }
        //swipe left
        if (swipe.x < 0 && swipe.y > -0.5f && swipe.y < 0.5f)
        {
            Debug.Log("left swipe");
            return PieceDirection.LEFT;
        }
        //swipe right
        if (swipe.x > 0 && swipe.y > -0.5f && swipe.y < 0.5f)
        {
            Debug.Log("right swipe");
            return PieceDirection.RIGHT;
        }
        return PieceDirection.STATIONARY;
    }

    // Find Match-3 Tile
    private List<TokensInfo> FindAMatch(ref ObjectsContatiner<TokensInfo> gameObjectContainer)
    {
        //creating an array to potentially store the matching tiles
        List<TokensInfo> matchedArray = new List<TokensInfo>();

        //Checking the vertical tiles
        for (int x = 0; x <= gameObjectContainer.GetContainerUpperBound(0); x++)
        {
            for (int y = 0; y <= gameObjectContainer.GetContainerUpperBound(1); y++)
            {
                //Get the current cell info from the object container
                TokensInfo current_tokenInfo = gameObjectContainer.GetFromContainer(x, y);

                //If it's an object marked as empty, continue
                if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece) 
                { 
                    continue;
                }

                //create local variables to be used
                int matchCount = 0;
                int y2 = gameObjectContainer.GetContainerUpperBound(1);
                int y1;

                //Getting the number of tiles of the same kind
                for (y1 = y + 1; y1 <= y2; y1++)
                {
                    // get object info
                    TokensInfo _tokenInfo = gameObjectContainer.GetFromContainer(x, y1);

                    if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                    {
                        break;
                    }

                    if (current_tokenInfo._tokenInfo.tokenType == _tokenInfo._tokenInfo.tokenType)
                    {
                        //Add match count if the pieces are the same shape
                        Debug.Log("Found Token Type Match! Name is " + _tokenInfo._tokenInfo.tokenGameObject.name);
                        matchCount++;
                    }
                    else if (current_tokenInfo._tokenInfo.tokenColour == _tokenInfo._tokenInfo.tokenColour)
                    {
                        //Add match count if the pieces are the same colour
                        Debug.Log("Found Token Origin  Match! Name is " + _tokenInfo._tokenInfo.tokenGameObject.name);
                        matchCount++;
                    }
                    else
                    {
                        break;
                    }
                }

                //If we found more than 2 tiles close we add them in the array of matching tiles
                if (matchCount >= 2)
                {
                    y1 = Mathf.Min(gameObjectContainer.GetContainerUpperBound(1), y1 - 1);
                    for (int y3 = y; y3 <= y1; y3++)
                    {
                        // get object info
                        TokensInfo _tokenInfo = gameObjectContainer.GetFromContainer(x, y3);

                        //check to see if the matched array already contains this piece
                        if (!matchedArray.Contains(_tokenInfo))
                        {
                            matchedArray.Add(_tokenInfo);
                        }
                    }
                }
            }
        }

        //Checking the horizontal tiles , in the following loops we will use the same concept as the previous ones
        for (int y = 0; y < gameObjectContainer.GetContainerUpperBound(1) + 1; y++)
        {
            for (int x = 0; x < gameObjectContainer.GetContainerUpperBound(0) + 1; x++)
            {
                //Get the current object info
                TokensInfo current_tokenInfo = gameObjectContainer.GetFromContainer(x, y);

                //If it's an object marked as empty or a null object, continue
                if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                {
                    continue;
                }

                //create local variables to be used
                int matchCount = 0;
                int x2 = gameObjectContainer.GetContainerUpperBound(0);
                int x1;

                //Getting the number of tiles of the same kind
                for (x1 = x + 1; x1 <= x2; x1++)
                {
                    // get object info
                    TokensInfo _tokenInfo = gameObjectContainer.GetFromContainer(x1, y);

                    if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                    {
                        break;
                    }

                    if (current_tokenInfo._tokenInfo.tokenType == _tokenInfo._tokenInfo.tokenType)
                    {
                        //Add match count if the pieces are the same shape
                        Debug.Log("Found Token Type Match! Name is " + _tokenInfo._tokenInfo.tokenGameObject.name);
                        matchCount++;
                    }
                    else if (current_tokenInfo._tokenInfo.tokenColour == _tokenInfo._tokenInfo.tokenColour)
                    {
                        //Add match count if the pieces are the same colour
                        Debug.Log("Found Token Origin  Match! Name is " + _tokenInfo._tokenInfo.tokenGameObject.name);
                        matchCount++;
                    }
                    else
                    {
                        break;
                    }
                }

                //If we found more than 2 tiles close we add them in the array of matching tiles
                if (matchCount >= 2)
                {
                    x1 = Mathf.Min(gameObjectContainer.GetContainerUpperBound(0), x1 - 1);
                    for (int x3 = x; x3 <= x1; x3++)
                    {
                        // get object info
                        TokensInfo _tokenInfo = gameObjectContainer.GetFromContainer(x3, y);

                        //check to see if the matched array already contains this piece
                        if (!matchedArray.Contains(_tokenInfo))
                        {
                            matchedArray.Add(_tokenInfo);
                        }
                    }
                }
            }
        }

        return matchedArray;
    }

    // Do Empty Tile Move Down by replacing the empty tiles with the ones above
    private void UpdateEmptySpaces(ref ObjectsContatiner<TokensInfo> gameObjectContainer)
    {   
        //First we loop through the grid
        for (int x = 0; x <= gameObjectContainer.GetContainerUpperBound(0); x++)
        {
            for (int y = 0; y <= gameObjectContainer.GetContainerUpperBound(1); y++)
            {
                //get the cell info from the pool
                TokensInfo current_tokenInfo = gameObjectContainer.GetFromContainer(x, y);

                if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                {
                    for (int y2 = y; y2 <= gameObjectContainer.GetContainerUpperBound(1); y2++)
                    {
                        //Get the next cell to move down. If next cell is empty we perform the replacement
                        TokensInfo next_tokenInfo = gameObjectContainer.GetFromContainer(x, y2);

                        if (!next_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                        {
                            //Add to the pool ASYNC
                            gameObjectContainer.AddToContainerASYNC(next_tokenInfo, x, y);

                            //Add to the pool ASYNC
                            gameObjectContainer.AddToContainerASYNC(current_tokenInfo, x, y2);
                            break;
                        }
                    }
                }
            }
        }

        //Instantiate new tiles to replace the ones destroyed
        for (int x = 0; x <= gameObjectContainer.GetContainerUpperBound(0); x++)
        {
            for (int y = 0; y <= gameObjectContainer.GetContainerUpperBound(1); y++)
            {
                //Get the current cell info
                TokensInfo current_tokenInfo = gameObjectContainer.GetFromContainer(x, y);

                if (current_tokenInfo._tokenInfo.bIsAnEmptyPiece)
                {
                    //Perform clean up
                    Destroy(current_tokenInfo._tokenInfo.tokenGameObject);

                    //Get a random piece from the list of prefabs
                    TokensInfo _randomPiece = listOfAvailablePieces[UnityEngine.Random.Range(0, listOfAvailablePieces.Length)];
                    
                    //Instantiate new game object piece for replacement
                    GameObject _Object = Instantiate(_randomPiece.gameObject, new Vector3(x, gameObjectContainer.GetContainerUpperBound(1) + 2, 0), transform.rotation);

                    //Update the main info
                    _randomPiece._tokenInfo.Init(x, y, _Object, false);

                    //Add to the object container
                    gameObjectContainer.AddToContainerASYNC(_randomPiece, _randomPiece._tokenInfo.posX, _randomPiece._tokenInfo.posY);
                }
            }
        }

        for (int x = 0; x <= gameObjectContainer.GetContainerUpperBound(0); x++)
        {
            for (int y = 0; y <= gameObjectContainer.GetContainerUpperBound(1); y++)
            {
                // Get the current cell info
                TokensInfo _tokenInfo = gameObjectContainer.GetFromContainer(x, y);

                //Perform destruction animation
                TweenParms parms = new TweenParms().Prop("position", new Vector3(x, y, -1)).Ease(EaseType.EaseOutQuart);
                HOTween.To(_tokenInfo._tokenInfo.tokenGameObject.transform, 0.4f, parms);
            }
        }
    }
}
