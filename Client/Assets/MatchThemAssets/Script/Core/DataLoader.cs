﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using WebSocketSharp;

public class DataLoader : MonoBehaviour
{
    public static string msettingsPath = "";
    public static string mitemsPath = "";

    //separator used to separate data in the text file
    static string[] separator = { "," };

    void Awake()
    {
#if UNITY_EDITOR
        msettingsPath = Path.Combine(Application.streamingAssetsPath, "PlayerSettings.txt");
        mitemsPath = Path.Combine(Application.streamingAssetsPath, "PlayersItems.txt");
#elif UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
        msettingsPath = Path.Combine(Application.persistentDataPath, "PlayerSettings.txt");
        mitemsPath = Path.Combine(Application.streamingAssetsPath, "PlayersItems.txt");
#endif
    }

    private static int StringToInt(string ToInt)
    {
        bool success = int.TryParse(ToInt, out int output);   // or use int.TryParse()

        if (success)
        {
            return output;
        }
        else
        {
            Debug.Log("Input string is invalid.");
        }
        return 0;
    }

    //public static async Task<string[]> LoadPlayerSettings()
    //{
    //    //Create the file if it doesn't exist
    //    HandleFiles.CreateFile(msettingsPath);

    //    //read the player settings
    //    string Data = await HandleFiles.ReadAllLinesAsync(msettingsPath);

    //    //Update the player settings data
    //    string[] _settingsArr = Data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

    //    //returns the updated data
    //    return _settingsArr;
    //}

    //public static async Task<List<ItemInfo>> LoadPlayerItems()
    //{
    //    //List that holds all the players items
    //    List<ItemInfo> _AllItemInfo = new List<ItemInfo>();

    //    //Create the file if it doesn't exist
    //    if (HandleFiles.CreateFile(mitemsPath))
    //    {
    //        //Create file with the header
    //        string _headers = "Name,PieceType,IconeName,HP,Damage,Bonus";

    //        //create local string arr to store the header
    //        string[] _newData = new string[1];

    //        //Update the array
    //        _newData[0] = _headers;

    //        //write to the file
    //        HandleFiles.WriteAllLinesAsync(mitemsPath, _newData);

    //        return _AllItemInfo;
    //    }

    //    //read the player settings
    //    string _data = await HandleFiles.ReadAllLinesAsync(mitemsPath);

    //    //Update the player settings data
    //    string[] _dataArr = _data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

    //    //Loop through each line to get and save the data from each line
    //    // we start from 1 because 0 contains the headers
    //    for (int i=1; i < _dataArr.Length; i++)
    //    {
    //        string _splitDataArr = _dataArr[i];

    //        if(!_splitDataArr.IsNullOrEmpty())
    //        {
    //            //Gets the item info from each line
    //            string[] _seperateData = _splitDataArr.Split(separator, StringSplitOptions.RemoveEmptyEntries);

    //            //Name of the item
    //            string _ItemName = _seperateData[0];

    //            //Piece type of the item
    //            GameInstance.GearPieceType _GearType = (GameInstance.GearPieceType)StringToInt(_seperateData[1]);

    //            //The name of the icon for this item
    //            string _IconName = _seperateData[2];

    //            //The item value used to give extra health to the player
    //            int _HPModifier = StringToInt(_seperateData[3]);

    //            //The item damage value when used to attack 
    //            int _Damage = StringToInt(_seperateData[4]);

    //            //if this item is bound to a colour, there would be a damage bonus added
    //            GameInstance.PiecesColour _DamageBonus = (GameInstance.PiecesColour)StringToInt(_seperateData[5]);

    //            //Create the item info data
    //            ItemInfo _itemInfo = new ItemInfo(_ItemName, _GearType, _IconName, null, _HPModifier, _Damage, _DamageBonus);

    //            if(_itemInfo != null)
    //            {
    //                //Add to the array
    //                _AllItemInfo.Add(_itemInfo);
    //            }
    //        }
    //    }
    //    return _AllItemInfo;
    //}
}
